'use strict';

describe('Directive: traceselection', function () {

  // load the directive's module
  beforeEach(module('notepadApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<traceselection></traceselection>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the traceselection directive');
  }));
});
