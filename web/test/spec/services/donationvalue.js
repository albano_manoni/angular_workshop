'use strict';

describe('Service: DonationValue', function () {

  // load the service's module
  beforeEach(module('notepadApp'));

  // instantiate service
  var DonationValue;
  beforeEach(inject(function (_DonationValue_) {
    DonationValue = _DonationValue_;
  }));

  it('should do something', function () {
    expect(!!DonationValue).toBe(true);
  });

});
