'use strict';

angular.module('notepadApp')
    .controller('MainCtrl', function($scope, $log, $modal) {

        $scope.newFile = function() {
            $log.info("Sending newFile signal");
            $scope.$broadcast('newFile');
        }

        $scope.openFile = function() {
            $log.info("Opening file modal");
            $scope.open();

        }

        $scope.saveFile = function() {
            $log.info("Sending saveFile signal");
            $scope.$broadcast('saveFile');
        }

        // Open file dialog
        $scope.open = function(size) {

            var modalInstance = $modal.open({
                templateUrl: 'openFileTemplate.html',
                controller: ModalInstanceCtrl,
                size: size,
                resolve: {}
            });

            modalInstance.result.then(function(document) {
                $log.info("About to broadcast: " + document.title);
                $scope.$broadcast('documentLoaded', document);
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        var ModalInstanceCtrl = function($scope, $log, $modalInstance, Document, Urlservice) {
            Document.query({}, function(data) {
                $scope.documents = data.objects;
            });

            $scope.deleteDocument = function(document) {
                var documentId = Urlservice.getResourceId(document.resource_uri);
                Document.remove({id: documentId}, function(data) {
                    $log.info("Document correctly deleted");
                    var index = $scope.documents.indexOf(document);
                    $scope.documents.splice(index, 1);
                });
            }

            $scope.ok = function(document) {
                $log.info("Selected document: " + document.title);
                $modalInstance.close(document);
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        };
    });