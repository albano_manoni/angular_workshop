'use strict';

angular.module('notepadApp')
    .controller('AboutCtrl', function($scope, $log, DonationValue, Donation, Urlservice) {
    	$scope.personalCard = "<address>Written by <a href=\"mailto:asswerus@gmail.com\">Albano \
    	Manoni</a>.<br> Visit my:<br><a href=\"https://it.linkedin.com/in/albanomanoni\" \
    	target=\"_blank\"> Linkedin Profile</a></address>";
        $scope.selectedValue = null;
        $scope.donationsAmount = 0;
        // Loading donation values
        loadDonationValues();
        loadDonations();



        $scope.makeDonation = function() {
            var donation = new Donation();
            donation.amount = $scope.selectedValue.resource_uri;
            donation.donation_date = new Date();
            Donation.save(donation, function(saved, responseHeaders) {
                $log.info("Document correctly saved to: " + responseHeaders('Location'));
                loadDonations();
                $scope.donationsAmount = calculateTotalAmount();
            });
        };

        // Utility functions
        function loadDonationValues() {
            DonationValue.query(function(data) {
                $scope.donationValues = data.objects;
            });
        }

        function loadDonations() {
            Donation.query(function(data) {
                $scope.donations = data.objects;
                $scope.donationsAmount = calculateTotalAmount();
            });
        }

        function calculateTotalAmount() {
            var total = 0;
            for (var x in $scope.donations) {
                var current = $scope.donations[x];
                total = total + current.amount.value;
            }
            return total;
        };

    });