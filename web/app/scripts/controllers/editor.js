'use strict';

angular.module('notepadApp')
    .controller('EditorCtrl', function($scope, $log, $modal, Document, Urlservice) {
        $scope.currentDocument = createEmptyDocument();

        // Signal target functions
        $scope.$on('newFile', function() {
            $log.info("Received newFile Signal");

            if (needSave()) {
                $scope.open('sm');
            } else {
                $scope.currentDocument = createEmptyDocument();
            }
        });

        $scope.$on('saveFile', function() {
            $log.info("Received saveFile Signal");
            saveDocument();
        });

        $scope.$on('textSelection', function(event, start, end, selectedText) {
            $log.info("Received selectedText signal. Selected text is: " + selectedText);
            $scope.currentDocument.selectionData.start = start;
            $scope.currentDocument.selectionData.stop = stop;
            $scope.currentDocument.selectionData.text = selectedText;

        });

        $scope.$on('documentLoaded', function(event, document) {
            $log.info("Loading the document" + document.title);
            $scope.currentDocument.id = Urlservice.getResourceId(document.resource_uri);
            $scope.currentDocument.title = document.title;
            $scope.currentDocument.content = document.content;
            $scope.currentDocument.isRemote = true;
            $scope.currentDocument.originalLength = calculateOriginaLength(document.content);
        });

        // Editor input changed handler
        $scope.onInputChanged = function() {
            $scope.currentDocument.pristine = false;
        };

        // Save dialog
        $scope.open = function(size) {

            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: ModalInstanceCtrl,
                size: size,
                resolve: {}
            });

            modalInstance.result.then(function(choice) {
                switch (choice) {
                    case 'saved':
                        saveDocument();
                        break;
                    case 'discard':
                        $scope.currentDocument = createEmptyDocument();
                        break;
                }

            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        var ModalInstanceCtrl = function($scope, $modalInstance) {


            $scope.save = function() {
                $modalInstance.close('saved');
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };

            $scope.confirm = function() {
                $modalInstance.close('discard');
            };
        };


        // Utility functions
        function saveDocument() {
            if (needSave()) {
                if ($scope.currentDocument.isRemote) {
                    updateResource($scope.currentDocument);
                } else {
                    createResource($scope.currentDocument);
                }
            }
        }

        function createEmptyDocument() {
            var emptyDocument = new Object();
            emptyDocument.id = 0;
            emptyDocument.title = "Unsaved";
            emptyDocument.content = "";
            emptyDocument.isRemote = false;
            emptyDocument.pristine = true;
            emptyDocument.originalLength = 0;

            var selectionData = new Object();
            selectionData.start = 0;
            selectionData.stop = 0;
            selectionData.text = "";
            emptyDocument.selectionData = selectionData;

            return emptyDocument;
        };

        function calculateOriginaLength(source) {
            return source.length;
        };

        function needSave() {
            return $scope.currentDocument.content != "" && !$scope.currentDocument.pristine 
            && $scope.currentDocument.content.length != $scope.currentDocument.originalLength;
        };

        function createResource(fromThis) {
            var resource = buildResource(fromThis);

            Document.save(resource, function(saved, responseHeaders) {
                $log.info("Document correctly saved to: " + responseHeaders('Location'));
            });
        };

        function updateResource(fromThis) {
            var resource = buildResource(fromThis);
            resource.id = fromThis.id;

            Document.update(resource, function(updated, responseHeaders) {
                $log.info("Document correctly updated");
            });
        };

        function buildResource(fromThis) {
            var resource = new Document();
            resource.title = fromThis.title;
            resource.content = fromThis.content;

            return resource;
        };

    });