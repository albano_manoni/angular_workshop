'use strict';

angular.module('notepadApp')
  .service('Urlservice', function Urlservice() {
        this.getResourceId = function(source) {
            return source.substring(source.lastIndexOf('/') + 1, source.length);
        }
        this.getLocation = function (source) {
            var baseLength = "http://localhost:8000/".length-1;
            return source.substring(baseLength, source.length);
        }

        this.contains = function (string, substr) {
            return string.indexOf(substr) != -1;
        }
    });
