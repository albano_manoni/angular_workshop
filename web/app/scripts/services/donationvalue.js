'use strict';

angular.module('notepadApp')
  .factory('DonationValue', function ($resource) {

    return $resource("http://127.0.0.1:8000/api/v1/donation_value/:id" , {
        format: 'json',
        limit: '40',
        id: '@id'
    }, {
        query: {
            method: 'GET',
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });

});
