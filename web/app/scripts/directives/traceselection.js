'use strict';

angular.module('notepadApp')
    .directive('traceselection', function($log) {
        return {
            restrict: 'A',
            link: function postLink(scope, element, attrs) {
                element.select(function(event) {
                    scope.$emit('textSelection', element.textrange().start, element.textrange().end, element.textrange().text);
                });
            }
        };
    });