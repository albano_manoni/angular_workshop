# README #

## Installing the environment ##

**Mysql**

A mysql server has to be installed and a database named **notepad** has to be present!!!

**Backend**

* For windows users:  follow this guide to install the backend environment [Installing Python, Pip and Virtualenv](https://zignar.net/2012/06/17/install-python-on-windows/)

* For linux users: python and pip are installed out of the box on linux, all you have to do is to open a shell and paste this: **sudo pip install virtualenv**

Create a virtual env with the command **virtualenv venv**

Move to the created folder **cd venv**

Activate the virtual env **source bin/activate** (linux) or **bin/activate.bat** (windows)

Move to the folder named **backend**

Execute **pip install -r requirements.txt**

**Frontend**

* Download and install Node.js [Node.js download page](http://nodejs.org/download/)

* Install Yeoman with the command **npm install -g yo**
* Move to the folder named **web** and execute **npm install**
* Move to the folder named **web** and execute **bower install**

## Starting the environment ##

In order to use and work with the example, you have to start either the backend and the frontend.

* Backend: activate the virtual environment (see backend installation section above)
* Inside the same shell, move to **backend/notepad_back/notepad_back**, open the file **settings.py** and modify the database section with your data.
* Move to the folder **backend/notepad_back** and execute **python manage.py syncdb**
* In the same folder, execute **python manage.py runserver**

The backend is now up and running.

**Frontend**

* Move to the folder called **web**, execute **grunt serve**

The frontend is now up and running.