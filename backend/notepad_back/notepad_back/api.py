import logging

from tastypie.authorization import Authorization
from tastypie import fields
from tastypie.resources import Resource, ModelResource, Bundle, ALL, ALL_WITH_RELATIONS

from models import *

class DocumentResource(ModelResource):

	class Meta:
		queryset = Document.objects.all()
		resource_name = 'document'
		excludes = ['id','resource_uri']
		authorization = Authorization()
		filtering = {
			'title': ALL,
			'content': ALL
			
		}

class DonationValueResource(ModelResource):

	class Meta:
		queryset = DonationValue.objects.all()
		resource_name = 'donation_value'
		excludes = ['id','resource_uri']
		authorization = Authorization()
		filtering = {
			'value': ALL,
			
		}

class DonationResource(ModelResource):
	amount = fields.ForeignKey('notepad_back.api.DonationValueResource', 'value', full=True)
	class Meta:
		queryset = Donation.objects.all()
		resource_name = 'donation'
		excludes = ['id','resource_uri']
		authorization = Authorization()
		filtering = {
			'value': ALL,
			
		}