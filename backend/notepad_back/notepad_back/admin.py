from django.contrib import admin
from models import *


class DocumentAdmin(admin.ModelAdmin):
	pass

class DonationValueAdmin(admin.ModelAdmin):
	pass

class DonationAdmin(admin.ModelAdmin):
	pass

admin.site.register(Document, DocumentAdmin)
admin.site.register(DonationValue, DonationValueAdmin)
admin.site.register(Donation, DonationAdmin)  