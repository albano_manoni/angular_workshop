from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from api import *

from tastypie.api import Api

notepad_api = Api(api_name='v1')
notepad_api.register(DocumentResource())
notepad_api.register(DonationValueResource())
notepad_api.register(DonationResource())

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'notepad_back.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(notepad_api.urls)),
)
