from django.db import models

class Document(models.Model):
	"""A text document"""
	title = models.CharField(max_length=300, null=False, blank=False)
	content = models.TextField()

	def __unicode__(self):
		return u'Title: %s' % (self.title)

class DonationValue(models.Model):
	"""Possible donation values """
	value = models.IntegerField();

	def __unicode__(self):
		return u'Value: %s' % (self.value)

class Donation(models.Model):
	"""Donations"""
	value = models.ForeignKey('DonationValue', related_name='donations')
	donation_date = models.DateField()


